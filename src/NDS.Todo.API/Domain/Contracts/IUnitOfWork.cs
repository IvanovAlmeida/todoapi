namespace NDS.Todo.API.Domain.Contracts;

public interface IUnitOfWork
{
    Task<bool> Commit();
}