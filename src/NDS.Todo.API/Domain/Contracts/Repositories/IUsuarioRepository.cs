using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Domain.Contracts.Repositories;

public interface IUsuarioRepository : IRepository<Usuario>
{
    Task<Usuario?> ObterPorEmail(string email);
    Task<bool> VerificarSeEmailEmUso(string email);
    void Cadastrar(Usuario usuario);
}