namespace NDS.Todo.API.Application.DTO.Usuarios;

public class RegistrarUsuarioDtoClass
{
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Senha { get; set; }
}

public record RegistrarUsuarioDto(string Nome, string Email, string Senha);