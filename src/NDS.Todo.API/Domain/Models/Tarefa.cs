namespace NDS.Todo.API.Domain.Models;

public class Tarefa : Entity
{
    public string Descricao { get; set; } = null!;
    public bool Concluido { get; set; }
    public DateTime? ConcluidoEm { get; set; }
    
    public Guid UsuarioId { get; set; }

    public virtual Usuario Usuario { get; set; } = null!;
}