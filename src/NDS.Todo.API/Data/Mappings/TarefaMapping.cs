using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Data.Mappings;

public class TarefaMapping : IEntityTypeConfiguration<Tarefa>
{
    public void Configure(EntityTypeBuilder<Tarefa> builder)
    {
        builder
            .HasKey(c => c.Id);

        builder
            .Property(c => c.Descricao)
            .HasMaxLength(180);

        builder
            .Property(c => c.Concluido)
            .IsRequired()
            .HasDefaultValue(false);

        builder
            .Property(c => c.ConcluidoEm)
            .IsRequired(false)
            .HasColumnType("DATETIME");
        
        builder
            .Property(c => c.UsuarioId)
            .IsRequired();
        
        builder
            .HasOne(t => t.Usuario)
            .WithMany(u => u.Tarefas)
            .HasForeignKey(c => c.UsuarioId)
            .OnDelete(DeleteBehavior.Cascade);
        
        builder
            .Property(c => c.CriadoEm)
            .ValueGeneratedOnAdd()
            .HasColumnType("DATETIME");
        
        builder
            .Property(c => c.AtualizadoEm)
            .ValueGeneratedOnAddOrUpdate()
            .HasColumnType("DATETIME");
    }
}