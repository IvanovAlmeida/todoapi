using System.Reflection;
using Microsoft.EntityFrameworkCore;
using NDS.Todo.API.Domain.Contracts;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Data.Context;

public class AppDbContext : DbContext, IUnitOfWork
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    { }

    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<Tarefa> Tarefas { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Vai aplicar uma configuração padrão para as propriedades que não forem mapeadas.
        // Setando como varchar(100)
        // Deve vir antes do *modelBuilder.ApplyConfigurationsFromAssembly()*
        foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
                     e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
            property.SetColumnType("varchar(100)");
        
        // Vai aplicar os mappings (Modelo -> Banco) usando reflection.
        // Os mappings ficam na pasta Mappings
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    public async Task<bool> Commit()
    {
        return await SaveChangesAsync() > 0;
    }
}