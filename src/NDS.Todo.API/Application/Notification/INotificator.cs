using FluentValidation.Results;

namespace NDS.Todo.API.Application.Notification;

public interface INotificator
{
    void Handle(string message);
    void Handle(List<ValidationFailure> failures);
    bool HasError { get; }
    IReadOnlyCollection<string> Errors { get; }
}