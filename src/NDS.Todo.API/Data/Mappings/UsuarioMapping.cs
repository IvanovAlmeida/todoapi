using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Data.Mappings;

public class UsuarioMapping : IEntityTypeConfiguration<Usuario>
{
    public void Configure(EntityTypeBuilder<Usuario> builder)
    {
        builder
            .HasKey(c => c.Id);

        builder
            .Property(c => c.Nome)
            .HasMaxLength(80)
            .IsRequired();

        builder
            .Property(c => c.Email)
            .HasMaxLength(80)
            .IsRequired();

        builder
            .Property(c => c.Senha)
            .HasMaxLength(255);

        builder
            .Ignore(c => c.Errors);
        
        builder
            .Property(c => c.CriadoEm)
            .ValueGeneratedOnAdd()
            .HasColumnType("DATETIME");
        
        builder
            .Property(c => c.AtualizadoEm)
            .ValueGeneratedOnAddOrUpdate()
            .HasColumnType("DATETIME");
    }
}