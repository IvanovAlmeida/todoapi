using System.Data;
using FluentValidation;
using NDS.Todo.API.Domain.Contracts.Repositories;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Domain.Validation;

public class UsuarioValidation : AbstractValidator<Usuario>
{
    private readonly IUsuarioRepository _usuarioRepository;

    public UsuarioValidation(IUsuarioRepository usuarioRepository)
    {
        _usuarioRepository = usuarioRepository;

        RuleFor(c => c.Nome)
            .NotEmpty()
            .MaximumLength(80);

        RuleFor(c => c.Email)
            .NotEmpty()
            .EmailAddress()
            .MaximumLength(180)
            .MustAsync(VerificarSeEmailEmUso)
                .WithMessage("O email já está em uso!");

    RuleFor(c => c.Senha)
            .NotEmpty()
            .MinimumLength(6);

        RuleFor(c => c)
            .Must(c => Check(c.Nome, c.Senha))
                .WithMessage("A senha não pode ser igual ao nome!");
    }

    private static bool Check(string nome, string senha)
    {
        return nome != senha;
    }

    private async Task<bool> VerificarSeEmailEmUso(string email, CancellationToken cancellationToken)
    {
        return !await _usuarioRepository.VerificarSeEmailEmUso(email);
    }
}