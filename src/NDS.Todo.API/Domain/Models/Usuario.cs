using FluentValidation.Results;
using NDS.Todo.API.Domain.Validation;

namespace NDS.Todo.API.Domain.Models;

public class Usuario : Entity
{
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Senha { get; set; }

    public virtual List<Tarefa> Tarefas { get; set; } = new();

    public List<ValidationFailure> Errors { get; private set; } = new();
    
    public bool EhValido()
    {
       //var validation = new UsuarioValidation().Validate(this);
       //Errors = validation.Errors;

       //return validation.IsValid;
       return true;
    }
}