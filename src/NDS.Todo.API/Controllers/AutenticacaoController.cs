using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NDS.Todo.API.Application.Contracts.Services;
using NDS.Todo.API.Application.DTO.Usuarios;
using NDS.Todo.API.Application.Notification;

namespace NDS.Todo.API.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class AutenticacaoController : Controller
{
    private readonly INotificator _notificator;
    private readonly IAutenticacaoService _autenticacaoService;
    
    public AutenticacaoController(IAutenticacaoService autenticacaoService, INotificator notificator)
    {
        _autenticacaoService = autenticacaoService;
        _notificator = notificator;
    }

    
    [HttpGet("check")]
    public ActionResult Check()
    {
        return Ok();
    }
    
    [AllowAnonymous]
    [HttpPost("registrar")]
    public async Task<IActionResult> Registrar([FromBody] RegistrarUsuarioDto dto)
    {
        var usuario = await _autenticacaoService.Registrar(dto);

        if (_notificator.HasError)
        {
            return BadRequest(_notificator.Errors);
        }
        
        return Ok(usuario);
    }
    
    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginUsuarioDto dto)
    {
        var autenticado = await _autenticacaoService.Autenticar(dto);

        if (_notificator.HasError)
        {
            return BadRequest(_notificator.Errors);
        }

        return Ok(autenticado);
    }
}