using Microsoft.EntityFrameworkCore;
using NDS.Todo.API.Data.Context;
using NDS.Todo.API.Domain.Models;
using NDS.Todo.API.Domain.Contracts;
using NDS.Todo.API.Domain.Contracts.Repositories;

namespace NDS.Todo.API.Data.Repositories;

public class UsuarioRepository : IUsuarioRepository
{
    private readonly AppDbContext _context;

    public IUnitOfWork UnitOfWork => _context;
    
    public UsuarioRepository(AppDbContext context)
    {
        _context = context;
    }

    public async Task<Usuario?> ObterPorEmail(string email)
    {
        return await _context
                    .Usuarios.FirstOrDefaultAsync(c => c.Email == email);
    }
    
    public async Task<bool> VerificarSeEmailEmUso(string email)
    {
        return await _context.Usuarios.AnyAsync(c => c.Email == email);
    }

    public void Cadastrar(Usuario usuario)
    {
        _context.Usuarios.Add(usuario);
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}