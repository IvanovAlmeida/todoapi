using FluentValidation.Results;

namespace NDS.Todo.API.Application.Notification;

public class Notificator : INotificator
{
    private readonly List<string> _errors = new();

    public void Handle(string message)
    {
        _errors.Add(message);
    }

    public void Handle(List<ValidationFailure> failures)
    {
        failures.ForEach(failure => Handle(failure.ErrorMessage));
    }

    public bool HasError => _errors.Any();
    public IReadOnlyCollection<string> Errors => _errors.AsReadOnly();
}