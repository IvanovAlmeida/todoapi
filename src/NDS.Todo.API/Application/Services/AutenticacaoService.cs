using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NDS.Todo.API.Application.Contracts.Services;
using NDS.Todo.API.Application.DTO.Usuarios;
using NDS.Todo.API.Application.Notification;
using NDS.Todo.API.Configurations;
using NDS.Todo.API.Domain.Contracts.Repositories;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Application.Services;

public class AutenticacaoService : IAutenticacaoService
{
    private readonly IMapper _mapper;
    private readonly IValidator<Usuario> _validator;
    private readonly IUsuarioRepository _usuarioRepository;
    private readonly IPasswordHasher<Usuario> _passwordHasher;
    private readonly INotificator _notificator;

    public AutenticacaoService(IMapper mapper, IValidator<Usuario> validator, IUsuarioRepository usuarioRepository, IPasswordHasher<Usuario> passwordHasher, INotificator notificator)
    {
        _mapper = mapper;
        _validator = validator;
        _usuarioRepository = usuarioRepository;
        _passwordHasher = passwordHasher;
        _notificator = notificator;
    }

    public async Task<UsuarioDto?> Registrar(RegistrarUsuarioDto dto)
    {
        var usuario = _mapper.Map<Usuario>(dto);
        var validacao = await _validator.ValidateAsync(usuario);

        if (!validacao.IsValid)
        {
            _notificator.Handle(validacao.Errors);
            return null;
        }

        #region Validação Antiga
        //if (!usuario.EhValido())
        //    throw new Exception("Tem erro");

        // Validar email
        //if (await _usuarioRepository.VerificarSeEmailEmUso(usuario.Email))
        //{
        //    throw new Exception("Email em uso!");
        //}
        #endregion
        
        // Encriptar a senha
        usuario.Senha = _passwordHasher.HashPassword(usuario, usuario.Senha);
        
        // Melhorar o retorno de erros - Notificator Pattern
        
        _usuarioRepository.Cadastrar(usuario);
        
        if (!await _usuarioRepository.UnitOfWork.Commit())
            throw new Exception("Erro ao salvar no banco");

        return _mapper.Map<UsuarioDto>(usuario);
    }

    public async Task<UsuarioAutenticadoDto?> Autenticar(LoginUsuarioDto dto)
    {
        // pegar usuario pelo email
        var usuario = await _usuarioRepository.ObterPorEmail(dto.Email);
        if (usuario == null)
        {
            _notificator.Handle("Usuário ou Senha incorretos!");
            return null;
        }
        
        // fazer check da senha
        var result = _passwordHasher.VerifyHashedPassword(usuario, usuario.Senha, dto.Senha);
        if (result == PasswordVerificationResult.Failed)
        {
            _notificator.Handle("Usuário ou Senha incorretos!");
            return null;
        }

        if (result == PasswordVerificationResult.SuccessRehashNeeded)
        {
            // Gera hash novamente e atualiza no banco
        }
        
        // gerar o jwt
        return new UsuarioAutenticadoDto
        {
            Id = usuario.Id,
            Email = usuario.Email,
            Nome = usuario.Nome,
            Token = GenerateToken(usuario)
        };
    }
    
    private static string GenerateToken(Usuario user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(Settings.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Nome),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim("MyClaim", "MyCustomClaim"),
            }),
            Expires = DateTime.UtcNow.AddHours(2),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}