using NDS.Todo.API.Application.DTO.Usuarios;

namespace NDS.Todo.API.Application.Contracts.Services;

public interface IAutenticacaoService
{
    Task<UsuarioDto?> Registrar(RegistrarUsuarioDto dto);
    Task<UsuarioAutenticadoDto?> Autenticar(LoginUsuarioDto dto);
}