using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Domain.Contracts.Repositories;

public interface IRepository<T> : IDisposable where T : Entity
{
    IUnitOfWork UnitOfWork { get; }
}