using AutoMapper;
using NDS.Todo.API.Application.DTO.Usuarios;
using NDS.Todo.API.Domain.Models;

namespace NDS.Todo.API.Configurations;

public class AutomapperProfile : Profile
{
    public AutomapperProfile()
    {
        CreateMap<Usuario, UsuarioDto>()
            .ReverseMap();
        
        CreateMap<Usuario, RegistrarUsuarioDto>()
            .ReverseMap();
    }
}