namespace NDS.Todo.API.Application.DTO.Usuarios;

public class UsuarioAutenticadoDto
{
    public Guid Id { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Token { get; set; }
}

public record LoginUsuarioDto(string Email, string Senha);